﻿using UnityEngine;
using System.Collections;

public class emitterActivater : MonoBehaviour
{

		public ParticleSystem[] particleSystems;
		public bool enabled = false;
		public float[] emissionRates;

		// Use this for initialization
		void Start ()
		{
				particleSystems = gameObject.GetComponentsInChildren<ParticleSystem> ();
				emissionRates = new float[particleSystems.Length];
				for (int i = 0; i < particleSystems.Length; i++) {
						emissionRates [i] = particleSystems [i].emissionRate;
				}

				//Disable by default.
				for (int i = 0; i < particleSystems.Length; i++) {
						particleSystems [i].emissionRate = 0;
				}
			

		}
	
		// Update is called once per frame
		void Update ()
		{
	
				if (Input.GetMouseButton (0) && !enabled) {
						//turn on
						enabled = true;
						for (int i = 0; i < particleSystems.Length; i++) {
								particleSystems [i].emissionRate = emissionRates [i];
						}
				} else if (!Input.GetMouseButton (0) && enabled) {
						//turn off
						enabled = false;
						for (int i = 0; i < particleSystems.Length; i++) {
								particleSystems [i].emissionRate = 0;
						}
				}

				
		}
}
