﻿using UnityEngine;
using System.Collections;

public class PlayerItemPoint : MonoBehaviour
{

		public bool isRight = true;
		private Transform player;

		// Use this for initialization
		void Start ()
		{
				player = transform.parent;
		}
	
		// Update is called once per frame
		void Update ()
		{

				//Determine which way the item point is relative to the player
				if (transform.position.x - player.position.x > 0) {
						isRight = true;
				} else {
						isRight = false;
				}
		}
}
