﻿using UnityEngine;
using System.Collections;

public class FollowPlayerItemPoint : MonoBehaviour
{

		private PlayerItemPoint itemPoint;

		// Use this for initialization
		void Start ()
		{
				GameObject player = GameObject.Find ("Weapon Hardpoint");
				itemPoint = player.GetComponent<PlayerItemPoint> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				transform.position = itemPoint.transform.position;

				Vector3 currentRotation = transform.rotation.eulerAngles;
				if (itemPoint.isRight) {
						currentRotation.y = 90;
				} else {
						currentRotation.y = -90;
				}
				transform.rotation = Quaternion.Euler (currentRotation);
		}
}
