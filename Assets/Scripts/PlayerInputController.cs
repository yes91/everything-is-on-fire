﻿using UnityEngine;
using System.Collections;


public class PlayerInputController : MonoBehaviour
{
		// movement config
		public float gravity = -25f;
		public float runSpeed = 8f;
		public float groundDamping = 20f; // how fast do we change direction? higher means faster
		public float inAirDamping = 5f;
		public float jumpHeight = 3f;

		[HideInInspector]
		private float
				normalizedHorizontalSpeed = 0;

		private CharacterController2D _controller;
		private Animator _animator;
		private RaycastHit2D _lastControllerColliderHit;
		private Vector3 _velocity;
		private Camera _camera;


		
		public bool canDoubleJump = true;
		private bool doubleJumped = false;

		public bool plungerEquiped = true;
		public float plungerHangTime = 1;
		public float plungerCooldown = 2;
		private bool wallPlunged = false;
		private float wallPlungTimer = 0;
		private float wallPlungTimerCooldown = 0;

		void Awake ()
		{
				_animator = GetComponent<Animator> ();
				_controller = GetComponent<CharacterController2D> ();
				_camera = GameObject.Find ("Main Camera").camera;

				// listen to some events for illustration purposes
				_controller.onControllerCollidedEvent += onControllerCollider;
				_controller.onTriggerEnterEvent += onTriggerEnterEvent;
				_controller.onTriggerExitEvent += onTriggerExitEvent;
		}


	#region Event Listeners

		void onControllerCollider (RaycastHit2D hit)
		{
				// bail out on plain old ground hits cause they arent very interesting
				if (hit.normal.y == 1f)
						return;

				// logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
				//Debug.Log ("flags: " + _controller.collisionState + ", hit.normal: " + hit.normal);
		}


		void onTriggerEnterEvent (Collider2D col)
		{
				Debug.Log ("onTriggerEnterEvent: " + col.gameObject.name);
		}


		void onTriggerExitEvent (Collider2D col)
		{
				Debug.Log ("onTriggerExitEvent: " + col.gameObject.name);
		}

	#endregion
		private static float Angle(Vector3 vec) {
			return (Mathf.Rad2Deg * -Mathf.Atan2( vec.y, vec.x )) / 180;
		}

		// the Update loop contains a very simple example of moving the character around and controlling the animation
		void Update ()
		{
				// grab our current _velocity to use as a base for all calculations
				_velocity = _controller.velocity;

				float angle = Angle (_camera.WorldToScreenPoint (transform.position) - Input.mousePosition);
				
				bool aiming = _controller.isGrounded && Input.GetKey (KeyCode.LeftShift);

				
				_animator.SetBool ("isAiming", aiming);
				if(transform.localScale.x > 0f) {
					_animator.SetFloat ("angle", angle);
				} else {
					_animator.SetFloat ("angle", 1f - angle);
				}

				if (aiming)
					return;

				if (_controller.isGrounded)
						_velocity.y = 0;
		
		if (Input.GetKey (KeyCode.D)) {
						normalizedHorizontalSpeed = 1;
						if (transform.localScale.x < 0f)
								transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);

						if (_controller.isGrounded) {

						}
				} else if (Input.GetKey (KeyCode.A)) {
						normalizedHorizontalSpeed = -1;
						if (transform.localScale.x > 0f)
								transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);

						if (_controller.isGrounded) {
								//_animator.Play (Animator.StringToHash ("Run"));
								
						}
				} else {
						normalizedHorizontalSpeed = 0;

						if (_controller.isGrounded) {
								//_animator.Play (Animator.StringToHash ("Idle"));
						}
				}


				// we can only jump whilst grounded
				if (_controller.isGrounded && Input.GetKeyDown (KeyCode.Space)) {
						_velocity.y = Mathf.Sqrt (2f * jumpHeight * -gravity);
						//_animator.Play (Animator.StringToHash ("Jump"));

				} else if (!_controller.isGrounded && Input.GetKeyDown (KeyCode.Space) && canDoubleJump && !doubleJumped) {
						//DOUBLE JUMP
						_velocity.y = Mathf.Sqrt (2f * jumpHeight * -gravity);
						//_animator.Play (Animator.StringToHash ("Jump"));
						doubleJumped = true;
				} else if (!_controller.isGrounded && _velocity.y < 0) {
						//Falling
						if ((_controller.collisionState.right && Input.GetKey (KeyCode.D) && plungerEquiped) ||
								(_controller.collisionState.left && Input.GetKey (KeyCode.A) && plungerEquiped)) {
								if (!wallPlunged && wallPlungTimerCooldown <= 0) {
										wallPlunged = true;
										wallPlungTimer = plungerHangTime;
								} else {
										//Already plunged to the wall.
										if (wallPlungTimer <= 0 && wallPlungTimerCooldown <= 0) {
												wallPlunged = false;
												wallPlungTimerCooldown = plungerCooldown;
										}
								}
						} else if (wallPlunged == true) {
								//We were plunged to the wall but not anymore
								wallPlunged = false;
								wallPlungTimerCooldown = plungerCooldown;
						}
				}

				//Reset double jump state if grounded.
				if (_controller.isGrounded) {
						doubleJumped = false;
				}	

				// apply horizontal speed smoothing it
				var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
				_velocity.x = Mathf.Lerp (_velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor);

				// apply gravity before moving
				if (wallPlunged) {
						_velocity.y = 0;
				} else {
						_velocity.y += gravity * Time.deltaTime;
						_controller.move (_velocity * Time.deltaTime);

				}

				//Update animation
				_animator.SetFloat ("speed_x", Mathf.Abs (_velocity.x));
				_animator.SetFloat ("speed_y", _velocity.y);
				_animator.SetBool ("isGrounded", _controller.isGrounded);
				_animator.SetBool ("isWallPlunged", wallPlunged);

				//Cooldowns
				if (wallPlungTimer > 0) {
						wallPlungTimer -= Time.deltaTime;
				}
				if (wallPlungTimerCooldown > 0) {
						wallPlungTimerCooldown -= Time.deltaTime;
				}
		}
}
